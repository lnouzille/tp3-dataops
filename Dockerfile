# Image de base avec Python
FROM python:3.8

# Installer poetry
RUN pip install poetry

# Répertoire de travail dans le conteneur 
WORKDIR /tp3-dataops

# Copiez les fichiers nécessaires dans le conteneur
COPY pyproject.toml poetry.lock /tp3-dataops/
COPY app /tp3-dataops/app/

# Installez les dépendances de votre projet avec poetry
RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi

# Commande pour démarrer votre application
CMD ["poetry", "run", "uvicorn", "app.app:app", "--host", "0.0.0.0", "--port", "8000"]