import uvicorn
import joblib
from fastapi import FastAPI
from app.encoding import encode_prenom
from sklearn.linear_model import LogisticRegression


# Créer une instance de l'application FastAPI
app = FastAPI()

# Importation du modèle
regr_load = joblib.load("app/model.v1.bin")

# Définir une route avec une méthode GET
@app.get("/")
def read_root():
    return {"message": "Hello, World!"}


@app.get("/predict/{prenom}")
def predict_prenom(prenom: str):
    prediction = int(regr_load.predict([encode_prenom(prenom)])[0])
    return {"prenom": prenom, "prediction": prediction}