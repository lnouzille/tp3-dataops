import pandas as pd 


def encode_prenom(prenom:str)->pd.Series:
    """ Encoder un prénom donnée en un pd.Series """
    alphabet = "abcdefghijklmnopqrstuvwxyzé'-"
    prenom = prenom.lower()
    return pd.Series([letter in prenom for letter in alphabet]).astype(int)